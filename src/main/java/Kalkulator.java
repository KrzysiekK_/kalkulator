import java.util.Scanner;

public class Kalkulator {

    public static void main (String[] args) {
        int liczba1;
        int liczba2;
        String znak;
        String odp;

        do {
            Scanner czytaj = new Scanner(System.in);

            System.out.println("Podaj pierwsza liczbe:");
            liczba1 = czytaj.nextInt();
            System.out.println("Podaj znak dzialania:");
            znak = czytaj.next();
            System.out.println("Podaj druga liczbe:");
            liczba2 = czytaj.nextInt();

            switch (znak) {
                case "+":
                    System.out.println("Wynik to: " + (liczba1 + liczba2));
                    break;
                case "-":
                    System.out.println("Wynik to: " + (liczba1 - liczba2));
                    break;
                case "*":
                    System.out.println("Wynik to: " + (liczba1 * liczba2));
                    break;
                case "/":
                    System.out.println("Wynik to: " + (liczba1 / liczba2));
                    break;
                default:
                    System.out.println("Podano nieprawidlowy znak. Dostepne znaki to +, -, *, /");
            }
            System.out.println("Kontynuowac? T/N");
            odp = czytaj.next();
        }
        while (odp.equalsIgnoreCase("T"));
        System.out.println("Do zobaczenia!");
    }
}